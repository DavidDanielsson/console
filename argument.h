#ifndef OPENGLWINDOW_PARAMETER_H
#define OPENGLWINDOW_PARAMETER_H

#include <string>
#include <sstream>

#include <glm/glm.hpp>

namespace
{
	const std::string TYPE_TO_STRING[9] = { "none", "bool", "int32", "int64", "uint64", "float", "double", "string", "unknown" };
}

struct Argument
{
public:
	Argument() {}
	Argument(const std::string& text)
		: value(text)
		, type(STRING)
	{}
	~Argument() = default;

	Argument& operator=(const std::string& rhs)
	{
		origin = "";
		value = rhs;
		type = TYPE::STRING;

		return *this;
	}

	enum TYPE { NONE = 0, BOOL, INT32, INT64, UINT64, FLOAT, DOUBLE, STRING, UNKNOWN };

	//"origin" is used to represent where "value" came from.
	//For instance, when the GetSet function is called the origin will be the variable's name.
	//When the Print function is called the origin will be "CommandPrint"
	std::string origin;
	std::string value;
	TYPE type;
};

//Console performs bounds checking (INT_MAX etc.), so there's no need to do it here
//Operators to convert a list of argument to primitive data types as well as string concats
inline bool operator>>(const std::vector<Argument>& lhs, bool& rhs)
{
	if(lhs.size() != 1)
		return false;

	rhs = std::atoi(&lhs.front().value.c_str()[0]) > 0;

	return true;
}

inline bool operator>>(const std::vector<Argument>& lhs, int8_t& rhs)
{
	if(lhs.size() != 1)
		return false;

	try
	{
		rhs = static_cast<int8_t>(stoi(lhs.front().value));
	}
	catch(std::out_of_range&)
	{
		if(lhs.front().value[0] == '-')
			rhs	= std::numeric_limits<int8_t>::min();
		else
			rhs = std::numeric_limits<int8_t>::max();
	}
	catch(std::invalid_argument&)
	{
		//No change
	}

	return true;
}

inline bool operator>>(const std::vector<Argument>& lhs, uint8_t& rhs)
{
	if(lhs.size() != 1)
		return false;

	try
	{
		rhs = static_cast<uint8_t>(stoi(lhs.front().value));
	}
	catch(std::out_of_range&)
	{
		if(lhs.front().value[0] == '-')
			rhs = std::numeric_limits<uint8_t>::min();
		else
			rhs = std::numeric_limits<uint8_t>::max();
	}
	catch(std::invalid_argument&)
	{
		//No change
	}

	return true;
}

inline bool operator>>(const std::vector<Argument>& lhs, int16_t& rhs)
{
	if(lhs.size() != 1)
		return false;

	try
	{
		rhs = static_cast<int16_t>(stoi(lhs.front().value));
	}
	catch(std::out_of_range&)
	{
		if(lhs.front().value[0] == '-')
			rhs = std::numeric_limits<int16_t>::min();
		else
			rhs = std::numeric_limits<int16_t>::max();
	}
	catch(std::invalid_argument&)
	{
		//No change
	}

	return true;
}

inline bool operator>>(const std::vector<Argument>& lhs, uint16_t& rhs)
{
	if(lhs.size() != 1)
		return false;

	try
	{
		rhs = static_cast<uint16_t>(stoi(lhs.front().value));
	}
	catch(std::out_of_range&)
	{
		if(lhs.front().value[0] == '-')
			rhs = std::numeric_limits<uint16_t>::min();
		else
			rhs = std::numeric_limits<uint16_t>::max();
	}
	catch(std::invalid_argument&)
	{
		//No change
	}

	return true;
}

inline bool operator>>(const std::vector<Argument>& lhs, int32_t& rhs)
{
	if(lhs.size() != 1)
		return false;

	try
	{
		rhs = static_cast<int32_t>(stoll(lhs.front().value));
	}
	catch(std::out_of_range&)
	{
		if(lhs.front().value[0] == '-')
			rhs = std::numeric_limits<int32_t>::min();
		else
			rhs = std::numeric_limits<int32_t>::max();
	}
	catch(std::invalid_argument&)
	{
		//No change
	}

	return true;
}

inline bool operator>>(const std::vector<Argument>& lhs, uint32_t& rhs)
{
	if(lhs.size() != 1)
		return false;

	try
	{
		rhs = static_cast<uint32_t>(stoll(lhs.front().value));
	}
	catch(std::out_of_range&)
	{
		if(lhs.front().value[0] == '-')
			rhs = std::numeric_limits<uint32_t>::min();
		else
			rhs = std::numeric_limits<uint32_t>::max();
	}
	catch(std::invalid_argument&)
	{
		//No change
	}

	return true;
}

inline bool operator>>(const std::vector<Argument>& lhs, int64_t& rhs)
{
	if(lhs.size() != 1)
		return false;

	try
	{
		rhs = static_cast<int64_t>(stoll(lhs.front().value));
	}
	catch(std::out_of_range&)
	{
		if(lhs.front().value[0] == '-')
			rhs = std::numeric_limits<int64_t>::min();
		else
			rhs = std::numeric_limits<int64_t>::max();
	}
	catch(std::invalid_argument&)
	{
		//No change
	}

	return true;
}

inline bool operator>>(const std::vector<Argument>& lhs, uint64_t& rhs)
{
	if(lhs.size() != 1)
		return false;

	try
	{
		rhs = static_cast<uint64_t>(stoull(lhs.front().value));
	}
	catch(std::out_of_range&)
	{
		if(lhs.front().value[0] == '-')
			rhs = std::numeric_limits<uint64_t>::min();
		else
			rhs = std::numeric_limits<uint64_t>::max();
	}
	catch(std::invalid_argument&)
	{
		//No change
	}

	return true;
}

inline bool operator>>(const std::vector<Argument>& lhs, float& rhs)
{
	if(lhs.size() != 1)
		return false;

	try
	{
		rhs = stof(lhs.front().value);
	}
	catch(std::out_of_range&)
	{
		if(lhs.front().value[0] == '-')
			rhs = std::numeric_limits<float>::min();
		else
			rhs = std::numeric_limits<float>::max();
	}
	catch(std::invalid_argument&)
	{
		//No change
	}

	return true;
}

inline bool operator>>(const std::vector<Argument>& lhs, double& rhs)
{
	if(lhs.size() != 1)
		return false;

	try
	{
		rhs = stod(lhs.front().value);
	}
	catch(std::out_of_range&)
	{
		if(lhs.front().value[0] == '-')
			rhs = std::numeric_limits<double>::min();
		else
			rhs = std::numeric_limits<double>::max();
	}
	catch(std::invalid_argument&)
	{
		//No change
	}

	return true;
}

inline void operator>>(const std::vector<Argument>& lhs, std::string& rhs)
{
	for(const Argument& argument : lhs)
		rhs += argument.value + ", ";

	if(rhs.size() > 0)
		rhs.erase(rhs.size() - 2, 2); //Remove ", "
}

inline void operator>>(bool lhs, std::vector<Argument>& rhs)
{
	Argument newArgument;

	newArgument.value = std::to_string(lhs);
	newArgument.type = Argument::BOOL;

	rhs.emplace_back(newArgument);
}

inline void operator>>(int8_t lhs, std::vector<Argument>& rhs)
{
	Argument newArgument;

	newArgument.value = std::to_string(lhs);
	newArgument.type = Argument::INT32;

	rhs.emplace_back(newArgument);
}

inline void operator>>(uint8_t lhs, std::vector<Argument>& rhs)
{
	Argument newArgument;

	newArgument.value = std::to_string(lhs);
	newArgument.type = Argument::INT32;

	rhs.emplace_back(newArgument);
}

inline void operator>>(int16_t lhs, std::vector<Argument>& rhs)
{
	Argument newArgument;

	newArgument.value = std::to_string(lhs);
	newArgument.type = Argument::INT32;

	rhs.emplace_back(newArgument);
}

inline void operator>>(uint16_t lhs, std::vector<Argument>& rhs)
{
	Argument newArgument;

	newArgument.value = std::to_string(lhs);
	newArgument.type = Argument::INT32;

	rhs.emplace_back(newArgument);
}

inline void operator>>(int32_t lhs, std::vector<Argument>& rhs)
{
	Argument newArgument;

	newArgument.value = std::to_string(lhs);
	newArgument.type = Argument::INT32;

	rhs.emplace_back(newArgument);
}

inline void operator>>(uint32_t lhs, std::vector<Argument>& rhs)
{
	Argument newArgument;

	newArgument.value = std::to_string(lhs);
	newArgument.type = Argument::INT64;

	rhs.emplace_back(newArgument);
}

inline void operator>>(int64_t lhs, std::vector<Argument>& rhs)
{
	Argument newArgument;

	newArgument.value = std::to_string(lhs);
	newArgument.type = Argument::INT64;

	rhs.emplace_back(newArgument);
}

inline void operator>>(uint64_t lhs, std::vector<Argument>& rhs)
{
	Argument newArgument;

	newArgument.value = std::to_string(lhs);
	newArgument.type = Argument::UINT64;

	rhs.emplace_back(newArgument);
}

inline void operator>>(float lhs, std::vector<Argument>& rhs)
{
	Argument newArgument;

	newArgument.value = std::to_string(lhs);
	newArgument.type = Argument::FLOAT;

	rhs.emplace_back(newArgument);
}

inline void operator>>(double lhs, std::vector<Argument>& rhs)
{
	Argument newArgument;

	newArgument.value = std::to_string(lhs);
	newArgument.type = Argument::DOUBLE;

	rhs.emplace_back(newArgument);
}

inline void operator>>(const std::string& lhs, std::vector<Argument>& rhs)
{
	Argument newArgument;

	newArgument.value = lhs;
	newArgument.type = Argument::STRING;

	rhs.emplace_back(newArgument);
}

inline bool operator>>(const std::vector<Argument>& lhs, glm::vec4& rhs)
{
	if(lhs.size() != 3 && lhs.size() != 4)
		return false;

	rhs.x = std::stof(lhs[0].value) * (1.0f / 255.0f);
	rhs.y = std::stof(lhs[1].value) * (1.0f / 255.0f);
	rhs.z = std::stof(lhs[2].value) * (1.0f / 255.0f);

	if(lhs.size() == 4)
		rhs.w = std::stof(lhs[3].value) * (1.0f / 255.0f);
	else
		rhs.w = 1.0f;

	return true;
}

inline bool operator>>(const glm::vec4& lhs, std::vector<Argument>& rhs)
{
	rhs.emplace_back(std::to_string(lhs.x * 255.0f));
	rhs.emplace_back(std::to_string(lhs.y * 255.0f));
	rhs.emplace_back(std::to_string(lhs.z * 255.0f));
	rhs.emplace_back(std::to_string(lhs.w * 255.0f));

	return true;
}

inline bool operator<<(std::stringstream& sstream, const glm::vec4& rhs)
{
	sstream << "(" + std::to_string(rhs.x) + ", " + std::to_string(rhs.y) + ", " + std::to_string(rhs.z) + ", " + std::to_string(rhs.w) + ")";

	return true;
}

inline bool operator>>(const std::vector<Argument>& lhs, glm::vec3& rhs)
{
	if(lhs.size() != 3 && lhs.size() != 3)
		return false;

	rhs.x = std::stof(lhs[0].value);
	rhs.y = std::stof(lhs[1].value);
	rhs.z = std::stof(lhs[2].value);

	return true;
}

inline bool operator>>(const glm::vec3& lhs, std::vector<Argument>& rhs)
{
	rhs.emplace_back(std::to_string(lhs.x));
	rhs.emplace_back(std::to_string(lhs.y));
	rhs.emplace_back(std::to_string(lhs.z));

	return true;
}

inline bool operator<<(std::stringstream& sstream, const glm::vec3& rhs)
{
	sstream << "(" + std::to_string(rhs.x) + ", " + std::to_string(rhs.y) + ", " + std::to_string(rhs.z) + ")";

	return true;
}

inline std::string operator+(const std::string& lhs, const Argument& rhs)
{
	return lhs + rhs.value;
}

inline std::string operator+(const Argument& lhs, const std::string& rhs)
{
	return lhs.value + rhs;
}

inline std::string operator+(const std::string& lhs, const std::vector<Argument>& rhs)
{
	std::string returnString = lhs;

	for(const Argument& argument : rhs)
		returnString += argument + ", ";

	returnString.erase(returnString.size() - 2);

	return returnString;
}

inline std::string operator+(const std::vector<Argument>& lhs, const std::string& rhs)
{
	std::string returnString;

	for(const Argument& argument : lhs)
		returnString += argument + "\n";

	returnString += rhs;

	return returnString;
}

inline std::string operator+=(std::string lhs, const Argument& rhs)
{
	lhs = lhs + rhs;
	return lhs;
}

inline std::string operator+=(const Argument& lhs, std::string rhs)
{
	rhs = lhs + rhs;
	return rhs;
}


#endif //OPENGLWINDOW_PARAMETER_H
