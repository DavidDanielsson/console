#ifndef OPENGLWINDOW_COMMANDGETSET_H
#define OPENGLWINDOW_COMMANDGETSET_H

#include "consoleVariable.h"

#include <sstream>

template<typename T>
class CommandGetSet
	: public ConsoleVariable
{
public:
	CommandGetSet(std::string name, T* value)
		: ConsoleVariable(name, false)
		, value(value)
	{ }

	~CommandGetSet() {}

	virtual std::vector<Argument> Execute(const ContextPointers* const contextPointers, const std::vector<Argument>& arguments) override
	{
		std::vector<Argument> returnArguments;

		//Use sstream since it allows for overloading of << and >> for custom classes
		if(arguments.empty())
		{
			//Get
			*value >> returnArguments;

			if(returnArguments.size() == 1)
				returnArguments[0].origin = name;
			else
			{
				for(int i = 0, end = static_cast<int>(returnArguments.size()); i < end; ++i)
					returnArguments[i].origin = name + "[" + std::to_string(i) + "]";
			}
		}
		else
		{
			//Set
			if(!(arguments >> *value))
			{
				returnArguments.emplace_back("Couldn't insert \"" + arguments + "\" into value. Check your input and/or data types.");
				returnArguments.back().origin = name;
			}
			else
			{
				std::stringstream stream;
				stream << name + " = ";
				stream << *value;

				returnArguments.emplace_back(stream.str());
				returnArguments.back().type = arguments.front().type;
				returnArguments.back().origin = name;
			}
		}

		return returnArguments;
	}

	std::string GetHelp() const override
	{
		return "Gets or sets a C++ variable";
	}
	std::string GetUsage() const override
	{
		return "Get: " + GetName() + "\nSet: " + GetName() + "(\\<data>)\nWhere data is the new data for the variable";
	}
	std::string GetExample() const override
	{
		return "Get: " + GetName() + "\nSet (assuming data type is float): " + GetName() + "(2.0f)";
	}

	std::string GetValue() const override
	{
		std::stringstream sstream;

		sstream << *value;

		return sstream.str();
	}

protected:
	T* value;
};

#endif //OPENGLWINDOW_COMMANDGETSET_H
