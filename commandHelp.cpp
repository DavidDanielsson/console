#include "commandHelp.h"

CommandHelp::CommandHelp(const std::string& name)
	: ConsoleCommand(name, true)
{
}

std::vector<Argument> CommandHelp::Execute(const ContextPointers* const contextPointers, const std::vector<Argument>& arguments)
{
	Argument returnArgument;

	if(arguments.size() == 0)
		returnArgument = GetHelpUsageExample();
	else if(arguments.size() == 1)
	{
		if(contextPointers->commandMap->count(arguments.front().value))
			returnArgument = "Printing help for " + arguments.front().value + ":\n" + contextPointers->commandMap->at(arguments.front().value)->GetHelpUsageExample();
		else
			returnArgument = "Couldn't print help for command \"" + arguments.front().value+ "\" since there is no such command";
	}
	else
		returnArgument = "Expected one or zero arguments, got " + std::to_string(arguments.size());

	std::vector<Argument> returnVector;
	returnArgument.origin = "HelpCommand";
	returnVector.emplace_back(returnArgument);
	return returnVector;
}

std::string CommandHelp::GetHelp() const
{
	return "Prints some helpful text so that you know what a command does and how to use it (just like this!)";
}

std::string CommandHelp::GetUsage() const
{
	return GetName() + "(\\<command>)\nWhere command is the command you want help with";
}

std::string CommandHelp::GetExample() const
{
	return "You seem to have this down already";
}