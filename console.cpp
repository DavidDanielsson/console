#include <limits>
#include <sstream>
#include <algorithm>
#include <cctype>

#include <dgui/button.h>
#include <dlib/input.h>
#include <dgui/colors.h>
#include <dgui/outlineBackground.h>
#include <dgui/emptyBackground.h>
#include <dgui/common.h>

#include "console.h"
#include "consoleVariable.h"

#include "commandHelp.h"
#include "commandDumpConsole.h"
#include "commandPrint.h"
#include "commandCallMethod.h"

Console::Console()
	: historyIndex(-1)
	, suggestionIndex(-1)
	, completeListMode(COMPLETE_LIST_MODE::HISTORY)
	, grabPosition(-1.0f, -1.0f)
	, contextPointers(&input, &output, &completeList, &commandDictionary, &commandMap, this)
{

}

Console::~Console()
{
	try
	{
		WriteAutoexec();
	}
	catch(std::exception& ex)
	{
		DLib::Logger::LogLine(DLib::LOG_TYPE_ERROR, "Caught exception in WriteAutoexec(): " + std::string(ex.what()));
	}
}

void Console::Init(DLib::Rect area, const std::shared_ptr<DGUI::GUIStyle>& style, const std::shared_ptr<DGUI::GUIBackground>& background, const std::shared_ptr<DGUI::GUIStyle>& backgroundStyle)
{
	if(!CastStyleTo<ConsoleStyle>(style.get(), DLib::LOG_TYPE_ERROR))
		return;

	this->style = std::dynamic_pointer_cast<ConsoleStyle>(style);

	if(this->style->characterSet == nullptr)
	{
		DLib::Logger::LogLine(DLib::LOG_TYPE_ERROR, "ConsoleStyle->characterSet is nullptr!");
		return;
	}

	minSize.x = this->style->padding.x * 2.0f + 128; //128 was chosen arbitrarily
	minSize.y = this->style->padding.y * 2.0f + this->style->characterSet->GetLineHeight() * 5 + this->style->inputOutputPadding;

	glm::vec2 newSize = area.GetSize() + this->style->padding * 2.0f;

	if(area.GetWidth() < minSize.x)
		newSize.x = minSize.x;
	if(area.GetHeight() < minSize.y)
		newSize.y = minSize.y;

	area.SetSize(newSize);

	//////////////////////////////////////////////////
	//Output
	//////////////////////////////////////////////////
	DLib::Rect outputRect;
	outputRect.SetPos(area.GetMinPosition() + this->style->padding);
	outputRect.SetSize(area.GetSize().x - this->style->padding.x * 2.0f
		, area.GetSize().y - this->style->padding.y * 2.0f - this->style->characterSet->GetLineHeight() - this->style->inputOutputPadding);

	output.Init(outputRect
	            , this->style->outputStyle
	            , this->style->outputBackground
	            , this->style->outputBackgroundStyle);

	output.allowEdit = true;

	//////////////////////////////////////////////////
	//Label
	//////////////////////////////////////////////////
	DLib::Rect labelRect;
	labelRect.SetPos(output.GetArea().GetMinPosition().x
		, output.GetArea().GetMaxPosition().y + this->style->inputOutputPadding);

	promptLabel.Init(labelRect, this->style->labelStyle, std::make_shared<DGUI::EmptyBackground>(), nullptr, this->style->labelText);

	//////////////////////////////////////////////////
	//Input
	//////////////////////////////////////////////////
	DLib::Rect inputRect;
	inputRect.SetPos(output.GetArea().GetMinPosition().x + promptLabel.GetArea().GetWidth()
		, output.GetArea().GetMaxPosition().y + this->style->inputOutputPadding);
	inputRect.SetSize(area.GetWidth() - this->style->padding.x * 2.0f, static_cast<float>(this->style->characterSet->GetLineHeight()));

	input.Init(inputRect
		, this->style->inputStyle
		, this->style->inputBackground
		, this->style->inputBackgroundStyle);

	input.SetJumpSeparators(" (),");

	area.SetSize(area.GetWidth(), output.GetArea().GetHeight() + input.GetArea().GetHeight() + this->style->padding.y * 2.0f + this->style->inputOutputPadding);

	//////////////////////////////////////////////////
	//CompelteList
	//////////////////////////////////////////////////
	completeList.Init(DLib::Rect::empty
	                  , this->style->completeListStyle
	                  , this->style->completeListBackground
	                  , this->style->completeListBackgroundStyle);

	UpdateCompleteListArea();
	HideCompleteList();

	manager.AddContainer(&input);
	manager.AddContainer(&output);
	manager.AddContainer(&completeList);

	DGUI::GUIContainer::Init(area, style, background, backgroundStyle);

	std::string helpCommandName = this->style->preferLowercaseFunctions ? "console_help" : "Console_Help";
	CommandHelp* commandHelp = new CommandHelp(helpCommandName);
	if(!AddCommand(commandHelp))
		delete commandHelp;

	std::string dumpCommandName = this->style->preferLowercaseFunctions ? "console_dump" : "Console_Dump";
	CommandDumpConsole* commandDumpConsole = new CommandDumpConsole(dumpCommandName);
	if(!AddCommand(commandDumpConsole))
		delete commandDumpConsole;

	std::string printCommandName = this->style->preferLowercaseFunctions ? "console_print" : "Console_Print";
	CommandPrint* commandPrint = new CommandPrint(printCommandName);
	if(!AddCommand(commandPrint))
		delete commandPrint;

	if(this->style->autoexecFile != "")
	{
		std::string addAutoexecWatchname = this->style->preferLowercaseFunctions ? "console_addAutoexecWatch" : "Console_AddAutoexecWatch";
		CommandCallMethod* AddAutoexecWatch = new CommandCallMethod(addAutoexecWatchname, std::bind(&Console::AddAutoexecWatchInternal, this, std::placeholders::_1), true);
		if(!AddCommand(AddAutoexecWatch))
			delete AddAutoexecWatch;

		std::string removeAutoexecWatchname = this->style->preferLowercaseFunctions ? "console_removeAutoexecWatch" : "Console_RemoveAutoexecWatch";
		CommandCallMethod* RemoveAutoexecWatch = new CommandCallMethod(removeAutoexecWatchname, std::bind(&Console::RemoveAutoexecWatchInternal, this, std::placeholders::_1), true);
		if(!AddCommand(RemoveAutoexecWatch))
			delete RemoveAutoexecWatch;

		std::string printAutoexecWatches = this->style->preferLowercaseFunctions ? "console_printAutoexecWatches" : "Console_PrintAutoexecWatches";
		CommandCallMethod* PrintAutoexecWatches = new CommandCallMethod(printAutoexecWatches, std::bind(&Console::PrintAutoexecWatchesInternal, this, std::placeholders::_1), true);
		if(!AddCommand(PrintAutoexecWatches))
			delete PrintAutoexecWatches;
	}
}

void Console::Update(std::chrono::nanoseconds delta)
{
	manager.Update(delta);

	if(DLib::Input::MouseMoved())
	{
		if(grabPosition.x != -1.0f)
		{
			if(move)
				SetPosition(DLib::Input::GetMousePosition() - grabPosition);
			else
			{
				SetSize(area.GetSize() + (DLib::Input::GetMousePosition() - grabPosition));
				grabPosition = DLib::Input::GetMousePosition();
			}
		}

		completeList.SetIgnoreMouse(false);
	}
}

void Console::DrawBackground(DLib::SpriteRenderer* spriteRenderer)
{
	background->Draw(spriteRenderer);

	output.DrawBackground(spriteRenderer);
	input.DrawBackground(spriteRenderer);
}

void Console::DrawMiddle(DLib::SpriteRenderer* spriteRenderer)
{
	output.DrawMiddle(spriteRenderer);
	input.DrawMiddle(spriteRenderer);
}

void Console::DrawForeground(DLib::SpriteRenderer* spriteRenderer)
{
	output.DrawForeground(spriteRenderer);
	input.DrawForeground(spriteRenderer);
	promptLabel.Draw(spriteRenderer);

	if(completeList.GetDraw())
		completeList.Draw(spriteRenderer);
}

bool Console::AddCommand(ConsoleCommand* command)
{
	if(VerifyCommandName(command->GetName()))
	{
		if(commandMap.find(command->GetName()) == commandMap.end())
		{
			commandMap.insert(std::make_pair(command->GetName(), std::unique_ptr<ConsoleCommand>(command)));
			commandDictionary.AddEntry(command->GetName());

			return true;
		}
		else
		{
			output.AddText("Tried to add already existing command \"" + command->GetName() + "\"");
			return false;
		}
	}

	output.AddText("Couldn't add command \"" + command->GetName() + "\" since it has an invalid name (only a-z, A-Z, 0-9, and _ allowed. Must start with a letter)");
	return false;
}

void Console::AddText(const string& text)
{
	output.AddText(text);
}

void Console::SetPosition(const glm::vec2& newPosition)
{
	SetPosition(newPosition.x, newPosition.y);
}

void Console::SetPosition(float x, float y)
{
	if(style->allowMove)
	{
		DGUI::GUIContainer::SetPosition(x, y);

		output.SetPosition(x + style->padding.x, y + style->padding.y);
		input.SetPosition(x + style->padding.x, output.GetArea().GetMaxPosition().y + this->style->inputOutputPadding);

		UpdateCompleteListPosition();
	}
}

void Console::SetSize(const glm::vec2& newSize)
{
	SetSize(newSize.x, newSize.y);
}

void Console::SetSize(float x, float y)
{
	if(style->allowResize)
	{
		HideCompleteList();

		if(x < minSize.x)
			x = minSize.x;
		if(y < minSize.y)
			y = minSize.y;

		DGUI::GUIContainer::SetSize(x, y);

		output.SetSize(x - style->padding.x * 2.0f, y - style->padding.y * 2.0f - input.GetSize().y - style->inputOutputPadding);
		input.SetSize(x - style->padding.x * 2.0f, input.GetArea().GetHeight());
		input.SetPosition(output.GetArea().GetMinPosition().x, output.GetArea().GetMaxPosition().y + style->inputOutputPadding);
	}
}

void Console::SetArea(const DLib::Rect& newArea)
{
	DGUI::GUIContainer::SetArea(newArea);
}

void Console::SetDraw(bool draw)
{
	GUIContainer::SetDraw(draw);

	if(draw)
	{
		input.Activate();
		output.Deactivate();
	}
}

void Console::Activate()
{
	SetDraw(true);

	recieveAllEvents = true;
}

void Console::Deactivate()
{
	SetDraw(false);

	recieveAllEvents = false;
}

bool Console::GetActive() const
{
	return recieveAllEvents;
}

void Console::Autoexec()
{
	ParseAutoexec();
}

bool Console::AddAutoexecWatch(const std::string& variable)
{
	if(commandMap.count(variable) == 0)
		return false;

	if(autoexecWatches.count(variable) == 0)
	{
		auto consoleVariable = dynamic_cast<ConsoleVariable*>(commandMap[variable].get());

		if(consoleVariable == nullptr)
			return false;

		autoexecWatches.emplace(variable, consoleVariable->GetValue());
	}

	return true;
}

bool Console::RemoveAutoexecWatch(const std::string& variable)
{
	if(autoexecWatches.count(variable) == 0)
		return false;

	autoexecWatches.erase(variable);
	return true;
}

void Console::PrintAutoexecWatches()
{
	for(auto pair : autoexecWatches)
		output.AddText(pair.first + "(" + pair.second + ")");
}

void Console::OnMouseEnter()
{
	if(draw)
		recieveAllEvents = true;
}

void Console::OnMouseExit()
{
}

void Console::OnMouseDown(const DLib::KeyState& keyState, const glm::vec2& mousePosition)
{
	if(area.Contains(mousePosition) || completeList.GetArea().Contains(mousePosition))
	{
		if(keyState.key == GLFW_MOUSE_BUTTON_RIGHT)
		{
			if(keyState.mods == 0)
			{
				grabPosition = mousePosition - area.GetMinPosition();
				move = true;
			}
			else
			{
				grabPosition = mousePosition;
				move = false;
			}
		}
		else
		{
			if(completeList.GetDraw()
				&& completeList.GetArea().Contains(mousePosition))
			{
				completeList.OnMouseDown(keyState, mousePosition);
			}
			else
				HideCompleteList();
		}
	}
	else
		recieveAllEvents = false;

	if(output.GetReceiveAllEvents())
		output.OnMouseDown(keyState, mousePosition);
	if(input.GetReceiveAllEvents())
		input.OnMouseDown(keyState, mousePosition);
}

void Console::OnMouseUp(const DLib::KeyState& keyState, const glm::vec2& mousePosition) //TODO: Test
{
	if(completeList.GetDraw()
	   && completeList.GetArea().Contains(mousePosition))
	{
		if(!completeList.GetIsScrolling())
		{
			DGUI::Button* button = static_cast<DGUI::Button*>(completeList.GetHighlitElement());

			AcceptText(button->GetText());
			HideCompleteList();
			input.Activate();
		}

		completeList.OnMouseUp(keyState, mousePosition);
	}
	else
	{
		if(output.GetReceiveAllEvents())
			output.OnMouseUp(keyState, mousePosition);
		if(input.GetReceiveAllEvents())
			input.OnMouseUp(keyState, mousePosition);
	}

	grabPosition.x = -1.0f;
}

void Console::OnKeyDown(const DLib::KeyState& keyState)
{
	std::string beforeEvent = input.GetText();

	if(output.GetReceiveAllEvents())
		output.OnKeyDown(keyState);
	if(input.GetReceiveAllEvents())
		input.OnKeyDown(keyState);

	switch(keyState.key)
	{
		case GLFW_KEY_UP:
			UpPressed();
			break;
		case GLFW_KEY_DOWN:
			DownPressed();
			break;
		case GLFW_KEY_LEFT:
		case GLFW_KEY_RIGHT:
			HideCompleteList();
			break;
		case GLFW_KEY_BACKSPACE:
			if(beforeEvent != input.GetText())
				BackspacePressed();
			break;
		case GLFW_KEY_DELETE:
			if(keyState.mods == GLFW_MOD_ALT) //TODO: This is kind of terrible, move to DeletePressed instead?
			{
				completeList.ClearElements();
				GenerateSuggestions(input.GetActiveCharacterBlockText());
				MoveSuggestionButtonsToCompleteList();
				ShowCompleteList();
			}
			else
			{
				if(beforeEvent != input.GetText())
					DeletePressed();
			}
			break;
		case GLFW_KEY_ENTER:
		case GLFW_KEY_KP_ENTER:
			EnterPressed();
			break;
		case GLFW_KEY_END:
		case GLFW_KEY_HOME:
			HideCompleteList();
			break;
		case GLFW_KEY_TAB:
			TabPressed();
			break;
		default:
			break;
	}
}

void Console::OnKeyUp(const DLib::KeyState& keyState)
{
	if(output.GetReceiveAllEvents())
		output.OnKeyUp(keyState);
	if(input.GetReceiveAllEvents())
		input.OnKeyUp(keyState);
	if(completeList.GetReceiveAllEvents())
		completeList.OnKeyUp(keyState);
}

void Console::OnChar(unsigned int keyCode)
{
	if(input.GetIsActive())
	{
		SwitchCompleteListMode(COMPLETE_LIST_MODE::COMPLETION);

		std::string textBeforeModification = input.GetActiveCharacterBlockText();

		input.OnChar(keyCode);

		std::string textAfterModification = GenerateSuggestionText();

		completeList.ClearElements();

		//If text was appended we only need to strip the suggestions,
		//otherwise a whole new set has to be generated
		if(!textBeforeModification.empty()
			&& textAfterModification.find(textBeforeModification) == 0)
			TrimSuggestions(textAfterModification);
		else
			GenerateSuggestions(textAfterModification);

		MoveSuggestionButtonsToCompleteList();
		ShowCompleteList();
	}
	else if(output.GetIsActive())
		output.OnChar(keyCode);
	if(completeList.GetReceiveAllEvents())
		completeList.OnChar(keyCode);
}

void Console::OnScroll(double x, double y)
{
	if(completeList.GetArea().Contains(DLib::Input::GetMousePosition()))
	{
		if(completeList.GetReceiveAllEvents())
			completeList.OnScroll(x, y);
	}
	else
	{
		if(output.GetReceiveAllEvents())
			output.OnScroll(x, y);
		if(input.GetReceiveAllEvents())
			input.OnScroll(x, y);
	}
}

bool Console::ParseAutoexec()
{
	if(style->autoexecFile == "")
		return true;

	output.AddText("Parsing autoexec...");

	std::ifstream in(style->autoexecFile);

	if(!in.is_open())
		return false;

	std::string line;
	for(int lineNr = 1; std::getline(in, line); ++lineNr)
	{
		line = TrimTextFrontBack(line);

		if(line.empty() || line.compare(0, 2, "//") == 0)
			continue;

		bool addWatch = false;
		if(line.compare(0, 6, "watch ") == 0)
		{
			addWatch = true;
			line = line.substr(6);
		}

		auto index = line.find('(');
		if(index == line.npos)
			index = line.find(' ');

		std::string function = line.substr(0, index);
		if(addWatch)
		{
			if(!AddAutoexecWatch(function))
			{
				DLib::Logger::LogLine(DLib::LOG_TYPE_WARNING, "Couldn't add \"" + function + "\" from line " + std::to_string(lineNr) + " to autoexec watches; there is no such variable");
				continue;
			}
		}

		autoexecData.emplace(function, lineNr);

		try
		{
			output.AddText(ExecuteFunction(line));
		}
		catch(std::invalid_argument& ex)
		{
			output.AddText("Error during autoexec on line " + std::to_string(lineNr) + ": " + ex.what());
		}
		catch(std::exception& ex)
		{
			output.AddText("Unknown exception caught during autoexec when trying to execute \"" + line + "\" on line " + std::to_string(lineNr) + ": " + ex.what());
		}
	}

	return true;
}

bool Console::WriteAutoexec()
{
	if(style->autoexecFile == "")
		return true;

	std::ofstream out(style->autoexecFile + ".tmp", std::ofstream::trunc);
	std::ifstream in(style->autoexecFile);

	if(!out.is_open())
	{
		DLib::Logger::LogLine(DLib::LOG_TYPE_WARNING, "Couldn't create temporary file \"" + style->autoexecFile + ".tmp\"" + ", can't write to autoexec");
		return false;
	}
	else if(!in.is_open())
	{
		DLib::Logger::LogLine(DLib::LOG_TYPE_WARNING, "Couldn't open autoexec file at \"" + style->autoexecFile + "\"");
		return false;
	}

	std::string line;
	for(int lineNr = 1; std::getline(in, line); ++lineNr)
	{
		line = TrimTextFrontBack(line);

		if(line.empty() || line.compare(0, 2, "//") == 0)
		{
			out << line << '\n';
			continue;
		}

		bool addWatch = false;
		if(line.compare(0, 6, "watch ") == 0)
		{
			addWatch = true;
			line = line.substr(6);
		}

		bool paren = true;
		auto index = line.find('(');
		if(index == line.npos)
		{
			index = line.find(' ');
			paren = false;
		}

		std::string function = line.substr(0, index);
		if(autoexecWatches.count(function) > 0
			&& addWatch)
		{
			if(paren)
				out << "watch " << function << "(" << autoexecWatches[function] << ")" << '\n';
			else
				out << "watch " << function << " " << autoexecWatches[function] << '\n';

			autoexecWatches.erase(function);
		}
		else
			out << (addWatch ? "watch " : "") << line << '\n';
	}

	for(auto pair : autoexecWatches)
		out << "watch " << pair.first << "(" << pair.second << ")" << '\n';

	in.close();
	out.close();

	std::remove(style->autoexecFile.c_str());
	std::rename((style->autoexecFile + ".tmp").c_str(), style->autoexecFile.c_str());

	return true;
}

std::vector<Argument> Console::AddAutoexecWatchInternal(const std::vector<Argument>& arguments)
{
	std::string returnString;

	int count = 0;
	for(const auto& argument : arguments)
	{
		if(AddAutoexecWatch(argument.value))
			++count;
		else
			returnString += "Couldn't add \"" + argument.value + "\" to autoexec since there is no such variable\n";
	}

	returnString += "Added " + std::to_string(count) + " variable" + (count > 1 ? "s" : "") + " to autoexec watches"; //Worth ternary

	std::vector<Argument> returnVector;
	returnString >> returnVector;
	return returnVector;
}

std::vector<Argument> Console::RemoveAutoexecWatchInternal(const std::vector<Argument>& arguments)
{
	std::vector<Argument> returnVector;

	int count = 0;
	for(const auto& argument : arguments)
	{
		if(RemoveAutoexecWatch(argument.value))
			++count;
	}

	"Removed " + std::to_string(count) + " variable" + (count > 1 ? "s" : "") + " from autoexec watches" >> returnVector;

	return returnVector;
}

std::vector<Argument> Console::PrintAutoexecWatchesInternal(const std::vector<Argument>& arguments)
{
	PrintAutoexecWatches();

	std::vector<Argument> returnVector;
	return returnVector;
}

void Console::UpPressed()
{
	if(input.GetReceiveAllEvents()
		|| completeList.GetReceiveAllEvents())
	{
		if(completeListMode == COMPLETE_LIST_MODE::HISTORY)
		{
			if(history.size() > 0)
			{
				if(completeList.GetDraw())
					historyIndex = completeList.GetHighlitElementIndex();

				if(historyIndex == -1)
					historyIndex = 0;
				else
				{
					historyIndex--;

					if(historyIndex == -1)
						historyIndex = static_cast<int>(history.size() - 1);
				}

				HighlightCompleteListIndex(historyIndex);

				ShowCompleteList();
			}
			else
				HideCompleteList();
		}
		else
		{
			if(suggestions.size() > 0)
			{
				if(completeList.GetDraw())
					suggestionIndex = completeList.GetHighlitElementIndex();

				if(suggestionIndex == -1)
					suggestionIndex = static_cast<int>(suggestions.size() - 1);
				else
				{
					suggestionIndex--;

					if(suggestionIndex == -1)
						suggestionIndex = static_cast<int>(suggestions.size() - 1);
				}

				HighlightCompleteListIndex(suggestionIndex);
				ShowCompleteList();
			}
			else
				HideCompleteList();
		}
	}
}

void Console::DownPressed()
{
	if(input.GetReceiveAllEvents()
		|| completeList.GetReceiveAllEvents())
	{
		if(completeListMode == COMPLETE_LIST_MODE::HISTORY)
		{
			if(history.size() > 0)
			{
				if(completeList.GetDraw())
					historyIndex = completeList.GetHighlitElementIndex();

				if(historyIndex == -1)
					historyIndex = 0;
				else
				{
					historyIndex++;

					if(historyIndex > static_cast<int>(history.size() - 1))
						historyIndex = 0;
				}

				HighlightCompleteListIndex(historyIndex);
				ShowCompleteList();
			}
			else
				HideCompleteList();
		}
		else
		{
			if(suggestions.size() > 0)
			{
				if(completeList.GetDraw())
					suggestionIndex = completeList.GetHighlitElementIndex();

				if(suggestionIndex == -1)
					suggestionIndex = 0;
				else
				{
					suggestionIndex++;
					suggestionIndex %= suggestions.size();
				}

				HighlightCompleteListIndex(suggestionIndex);
				ShowCompleteList();
			}
			else
				HideCompleteList();
		}
	}
}

void Console::EnterPressed()
{
	if(!input.GetIsEmpty())
	{
		if(historyIndex == -1)
			AddToHistoryIfNeeded(input.GetText());
		else
			MoveToFrontOfHistory(historyIndex);

		std::string text = input.GetText();

		try
		{
			if(text.find_first_not_of(' ') != text.npos)
				output.AddText(ExecuteFunction(text));
		}
		catch(std::invalid_argument& ex)
		{
			output.AddText(ex.what());
		}
		catch(std::exception& ex)
		{
			output.AddText("An unknown exception was caught when trying to execute\"" + text + "\": " + std::string(ex.what()));
		}
			
		unsigned int linesAfter = output.GetLineCount();

		if(linesAfter > style->maxLines
			&& !style->dumpFile.empty())
		{
			std::ofstream out(style->dumpFile, std::ofstream::app);

			std::vector<std::string> lineRange = output.GetLines(0, linesAfter - style->maxLines);
			output.EraseLines(0, linesAfter - style->maxLines);

			for(auto line : lineRange)
				out << line << '\n';
		}

		input.SetText("");

		//Doesn't directly call SwitchCompleteListModes but the end result is the same.
		//Keep these comments in case someone searches for SwitchCompleteList usages
		//SwitchCompleteListMode(COMPLETE_LIST_MODE::HISTORY);
		completeListMode = COMPLETE_LIST_MODE::HISTORY;

		HideCompleteList();
		MoveHistoryButtonsToCompleteList();

		historyIndex = -1;
		suggestionIndex = -1;
	}
}

void Console::BackspacePressed()
{
	if(input.GetIsEmpty())
	{
		SwitchCompleteListMode(COMPLETE_LIST_MODE::HISTORY);

		historyIndex = -1;
		suggestionIndex = -1;

		HideCompleteList();
	}
	else
	{
		SwitchCompleteListMode(COMPLETE_LIST_MODE::COMPLETION);

		GenerateSuggestions(input.GetActiveCharacterBlockText());
		MoveSuggestionButtonsToCompleteList();
		ShowCompleteList();
	}
}

void Console::DeletePressed()
{
	if(input.GetIsEmpty())
	{
		SwitchCompleteListMode(COMPLETE_LIST_MODE::HISTORY);

		historyIndex = -1;
		suggestionIndex = -1;

		HideCompleteList();
	}
	else
		SwitchCompleteListMode(COMPLETE_LIST_MODE::COMPLETION);

	GenerateSuggestions(input.GetActiveCharacterBlockText());
	MoveSuggestionButtonsToCompleteList();
	ShowCompleteList();
}

void Console::TabPressed()
{
	if(completeList.GetDraw()
		&& completeList.GetElementsSize() > 0)
	{
		if(historyIndex != -1
			|| suggestionIndex != -1)
			HighlightCompleteListIndex(std::max(historyIndex, suggestionIndex));
		else
			HighlightCompleteListIndex(0);
	}
}

std::string Console::ExecuteFunction(std::string text)
{
	std::string trimmed = TrimTextFrontBack(text);

	//Anything with quotes around it (") will be regarded as one single argument
	//Any variable/function wrapped in quotes will be sent as-is as a string
	//Any variable/function not warpped in quotes will be resolved
	//Any argument that is a function call needs to use parentheses to mark its parameter list

	//First comes the function to call, then a space to mark parameters
	std::string function;
	std::string parameterList;

	size_t parenIndex = trimmed.find('(');

	if(parenIndex != trimmed.npos)
	{
		if(trimmed.back() != ')')
			throw std::invalid_argument("Evaluated \"" + text + "\" to be a function call with parameters, but no closing ) was found");

		trimmed = TrimText(trimmed);

		function = trimmed.substr(0, trimmed.find('('));
		parameterList = trimmed.substr(trimmed.find('(') + 1);

		parameterList.pop_back(); //Remove ")"
	}
	else if(trimmed.find(' ') != trimmed.npos)
	{
		function = trimmed.substr(0, trimmed.find(' '));

		parameterList = TrimText(trimmed.substr(trimmed.find_first_not_of(' ', trimmed.find(' '))));
	}
	else
		function = trimmed; //No parameters

	if(commandMap.find(function) == commandMap.end())
		throw std::invalid_argument("Evaluated \"" + trimmed + "\" to be a function call, but no function named \"" + function + "\" was found");

	//function
	try
	{
		std::vector<std::string> textArguments = SplitArg(parameterList, ',');

		std::vector<Argument> arguments;
		if(commandMap[function]->GetForceStringArguments())
		{
			for(const std::string& argument : textArguments)
			{
				if(argument.front() == '\"')
					arguments.emplace_back(argument.substr(1, argument.size() - 2));
				else
					arguments.emplace_back(argument);
			}
		}
		else
		{
			for(const std::string& argument : textArguments)
			{
				for(Argument returnValue : EvaluateExpression(argument))
					arguments.emplace_back(returnValue);
			}
		}
		
		std::string argumentsString;

		commandMap[function]->Execute(&contextPointers, arguments) >> argumentsString;

		if(autoexecWatches.count(function) > 0)
			autoexecWatches[function] = std::move(parameterList);

		return argumentsString;
	}
	catch(std::invalid_argument& ex)
	{
		return ex.what();
	}
}

std::vector<Argument> Console::ExecuteArgumentFunction(std::string text)
{
	//See ExecuteArguments() for comments
	std::string trimmed = TrimText(text);

	std::string function;
	std::string parameterList;

	if(trimmed.find('(') == trimmed.npos)
	{
		if(trimmed.find(' ') != trimmed.npos)
			throw std::invalid_argument("Evaluated \"" + text + "\" to be a function call without parameters, but a space was found");

		function = trimmed;
	}
	else
	{
		if(trimmed.back() != ')')
			throw std::invalid_argument("Evaluated \"" + text + "\" to be a function call with parameters, but no closing ) was found");

		function = trimmed.substr(0, trimmed.find('('));
		parameterList = trimmed.substr(trimmed.find('(') + 1);

		parameterList.pop_back();
	}

	if(commandMap.find(function) == commandMap.end())
		throw std::invalid_argument("Evaluated \"" + trimmed + "\" to be a function call, no such function was found");

	try
	{
		std::vector<std::string> textArguments = SplitArg(parameterList);

		std::vector<Argument> arguments;
		if(commandMap[function]->GetForceStringArguments())
		{
			for(const std::string& argument : textArguments)
			{
				if(argument.front() == '\"')
					arguments.emplace_back(argument.substr(1, argument.size() - 2));
				else
					arguments.emplace_back(argument);
			}
		}
		else
		{
			for(const std::string& argument : textArguments)
			{
				for(Argument returnValue : EvaluateExpression(argument))
					arguments.emplace_back(returnValue);
			}
		}

		return commandMap[function]->Execute(&contextPointers, arguments);
	}
	catch(std::invalid_argument& ex)
	{
		Argument exceptionArgument;

		exceptionArgument.value = ex.what();
		exceptionArgument.type = Argument::TYPE::STRING;
		exceptionArgument.origin = "invalid_argument exception";

		std::vector<Argument> returnArguments;
		returnArguments.emplace_back(exceptionArgument);

		return returnArguments;
	}
}

std::vector<std::string> Console::SplitArg(std::string args, char splitBy /* = ' '*/)
{
	std::vector<std::string> returnVector;

	if(args.empty())
		return returnVector;

	bool insideString = false;

	unsigned int lastCharacter = 0;

	int depth = 0;

	//Print(Print("asdf",Print("asdf","asdf")),wireframe)
	//	Print("asdf",Print("asdf","asdf")),wireframe
	
	//	Print("asdf",Print("asdf","asdf"))
	//	wireframe

	std::string currentArgument;

	bool add = true;

	for(int i = 0, end = args.size(); i < end; ++i)
	{
		if(args[i] == '"' && lastCharacter != '\\')
			insideString = !insideString;
		else if(!insideString)
		{
			if(args[i] == '(')
			{
				++depth;
			}
			else if(args[i] == ')')
			{
				--depth;
			}
			else if(args[i] == ',')
			{
				if(depth == 0)
				{
					returnVector.emplace_back(std::move(currentArgument));
					currentArgument = "";
					add = false;
				}
			}
		}
		
		if(add)
			currentArgument += args[i];
		else
			add = true;

		lastCharacter = static_cast<unsigned int>(args[i]);
	}

	returnVector.emplace_back(currentArgument);

	return returnVector;
}

std::vector<Argument> Console::EvaluateExpression(std::string expression)
{
	Argument returnArgument;
	returnArgument.value = expression;

	if(expression.size() == 0)
		returnArgument.type = Argument::NONE;
	else
	{
		//There are three different things to extract:
		//"adsf" => string, asdf
		//someFunction(parameters) => call function and return its return value
		//numbers => int32/int64/uint64/float/double, number
		if(expression[0] == '"')
		{
			//string
			for(int i = 1, end = static_cast<int>(expression.size() - 1); i < end; ++i)
			{
				auto character = expression[i];

				if(character == '"' && expression[i - 1] != '\\')
					throw std::invalid_argument("Evaluated \"" + expression + "\" to be a string, but some additional quote (\") was found");
				else if(character == '\\' && expression[i + 1] == '"')
				{
					expression.erase(i, 1);
					--end;
				}
			}

			if(expression.back() != '"')
				throw std::invalid_argument("Evaluated \"" + expression + "\" to be a string, but no closing quote was found");

			returnArgument.type = Argument::STRING;
			returnArgument.value = expression.substr(1, expression.size() - 2);
		}
		else if(std::isalpha(expression[0]))
		{
			if(expression.back() == ',')
				expression.pop_back();

			return ExecuteArgumentFunction(expression);
		}
		else
		{
			//int32/int64/uint64/float/double
			std::stringstream sstream(expression);

			if(expression.back() == 'f')
			{
				if(TryParse<float>(sstream))
				{
					returnArgument.type = Argument::FLOAT;
					returnArgument.value = expression.substr(0, expression.size() - 1);
				}
				else
					throw std::invalid_argument("Evaluated \"" + expression + "\" to be a float, but extraction failed");
			}
			else if(expression.find('.') != expression.npos)
			{
				if(std::count(expression.begin(), expression.end(), '.') == 1)
				{
					if(TryParse<double>(sstream))
						returnArgument.type = Argument::DOUBLE;
					else
						throw std::invalid_argument("Evaluated \"" + expression + "\" to be a double, but extraction failed");
				}
				else
					throw std::invalid_argument("Evaluated \"" + expression + "\" to be a double, but more than one decimal point was found");
			}
			else
			{
				for(char character : expression)
				{
					if(!std::isdigit(character)
					   && character != '-'
					   && character != '+')
						throw std::invalid_argument("Evaluated \"" + expression + "\" to be an integer, but " + std::to_string(character) + " was not a number or a ±");
				}

				if(TryParse<int32_t>(sstream))
					returnArgument.type = Argument::INT32;
				else if(TryParse<int64_t>(sstream))
					returnArgument.type = Argument::INT64;
				else if(TryParse<uint64_t>(sstream))
					returnArgument.type = Argument::UINT64;
				else
					throw std::invalid_argument("Evaluated \"" + expression + "\" to be a integer, but extraction failed");
			}
		}
	}

	std::vector<Argument> returnArguments;
	returnArguments.emplace_back(returnArgument);

	return returnArguments;
}

std::string Console::TrimText(string text)
{
	if(text.size() == 0)
		return "";

	text = TrimTextFrontBack(text);

	std::string returnText;

	unsigned int lastCharacter = 0;

	//Remove any spaces between arguments etc.
	bool inside = false; //If the character is within quotes
	for(int i = 0, end = static_cast<int>(text.size()); i < end; ++i)
	{
		auto character = text[i];

		if(character == '"' && lastCharacter != '\\')
		{
			inside = !inside;

			returnText += character;
		}
		else if(character == ' ')
		{
			if(inside)
				returnText += character;
		}
		else
			returnText += character;

		lastCharacter = static_cast<unsigned int>(character);
	}

	return returnText;
}

std::string Console::TrimTextFrontBack(const string& text)
{
	size_t firstNotOf = text.find_first_not_of(" \t");
	if(firstNotOf == text.npos)
		return text;

	size_t lastNotOf = text.find_last_not_of(" \t");

	return text.substr(firstNotOf, lastNotOf - firstNotOf + 1);
}

void Console::AddToHistoryIfNeeded(const std::string& text)
{
	auto iter = std::find(history.begin(), history.end(), text);
	if(iter != history.end())
		MoveToFrontOfHistory(iter);
	else
		AddToHistory(text);
}

void Console::AddToHistory(const std::string& text)
{
	if(history.size() == style->historySize)
	{
		history.pop_back();

		history.emplace_front(text);

		if(style->historySize > 0)
		{
			DGUI::Button* lastButton = static_cast<DGUI::Button*>(historyButtons.back().release());

			lastButton->SetText(text);

			historyButtons.pop_back();
			historyButtons.insert(historyButtons.begin(), std::unique_ptr<DGUI::Button>(lastButton));
		}
	}
	else
	{
		history.emplace_front(text);

		DGUI::Button* button = new DGUI::Button;

		button->Init(DLib::Rect(glm::vec2(), glm::vec2(style->completeListBackground->GetWorkArea().GetWidth(), this->style->characterSet->GetLineHeight()))
					 , style->completeListButtonStyle
					 , std::shared_ptr<DGUI::GUIBackground>(style->completeListButtonBackground->Clone())
					 , style->completeListButtonBackgroundStyle
					 , nullptr
					 , history.front());

		historyButtons.insert(historyButtons.begin(), std::unique_ptr<DGUI::Button>(button));
	}
}

void Console::MoveToFrontOfHistory(int index)
{
	std::string newFront = history[index];

	history.erase(history.begin() + index);
	history.emplace_front(newFront);

	std::unique_ptr<DGUI::GUIContainer> newFirst = std::unique_ptr<DGUI::GUIContainer>(historyButtons[index].release());

	historyButtons.erase(historyButtons.begin() + index);
	historyButtons.insert(historyButtons.begin(), std::move(newFirst));
}

void Console::MoveToFrontOfHistory(std::deque<std::string>::iterator iter)
{
	MoveToFrontOfHistory(static_cast<int>(std::distance(history.begin(), iter)));
}

void Console::MoveHistoryButtonsToCompleteList()
{
	completeList.ClearElements();

	std::vector<DGUI::GUIContainer*> completeListElements;
	completeListElements.reserve(historyButtons.size());

	for(const auto& pointer : historyButtons)
		completeListElements.push_back(pointer.get());

	UpdateCompleteListArea();
	completeList.SetElements(std::move(completeListElements));
}

void Console::MoveSuggestionButtonsToCompleteList()
{
	completeList.ClearElements();

	std::vector<DGUI::GUIContainer*> completeListElements;
	completeListElements.reserve(suggestionButtons.size());

	for(const auto& pointer : suggestionButtons)
		completeListElements.push_back(pointer.get());

	UpdateCompleteListArea();
	completeList.SetElements(std::move(completeListElements));
}

void Console::HideCompleteList()
{
	completeList.SetReceiveAllEvents(false);
	completeList.SetDraw(false);
}

void Console::ShowCompleteList()
{
	completeList.SetReceiveAllEvents(true);
	completeList.SetDraw(true);
}

void Console::UpdateCompleteListArea()
{
	UpdateCompleteListSize();
	UpdateCompleteListPosition();
}

void Console::UpdateCompleteListPosition()
{
	DLib::Rect newArea = completeList.GetArea();
	DLib::Rect compListArea = completeList.GetArea();

	float newHeight = -1.0f;

	glm::vec2 windowSize = DLib::Input::GetWindowSize();
	glm::vec2 newPosition = glm::vec2(input.GetArea().GetMinPosition().x, input.GetArea().GetMaxPosition().y);
	newArea.SetPos(newPosition);

	if(newArea.GetMinPosition().x + compListArea.GetWidth() > windowSize.x)
	{
		newPosition.x -= newArea.GetMaxPosition().x - windowSize.x;

		if(newPosition.x < 0)
			newPosition.x = 0;
	}

	DGUI::OutlineBackgroundStyle* outlineStyle = dynamic_cast<DGUI::OutlineBackgroundStyle*>(style->completeListBackgroundStyle.get());
	if(outlineStyle != nullptr)
	{
		outlineStyle->outlineSides = DGUI::DIRECTIONS::BOTTOM | DGUI::DIRECTIONS::LEFT | DGUI::DIRECTIONS::RIGHT;
		static_cast<DGUI::OutlineBackground*>(style->completeListBackground.get())->UpdateOutlineRect();
	}

	if(newArea.GetMinPosition().y + compListArea.GetHeight() > windowSize.y)
	{
		//Put list on top of box if there is room.
		//Othwerwise cap height
		if(input.GetArea().GetMinPosition().y - compListArea.GetHeight() >= 0)
		{
			newPosition.y = input.GetArea().GetMinPosition().y - compListArea.GetHeight();

			if(outlineStyle != nullptr)
			{
				outlineStyle->outlineSides = DGUI::DIRECTIONS::TOP | DGUI::DIRECTIONS::LEFT | DGUI::DIRECTIONS::RIGHT;
				static_cast<DGUI::OutlineBackground*>(style->completeListBackground.get())->UpdateOutlineRect();
			}
		}
		else
		{
			newHeight = windowSize.y - newPosition.y;
			newHeight -= static_cast<int>(newHeight) % style->characterSet->GetLineHeight();
		}
	}

	if(newHeight != -1.0f)
		newArea.SetSize(compListArea.GetWidth(), newHeight);

	completeList.SetPosition(newPosition);
}

void Console::UpdateCompleteListSize()
{
	glm::vec2 newSize;

	if(completeListMode == COMPLETE_LIST_MODE::HISTORY)
		newSize.y = static_cast<float>(style->characterSet->GetLineHeight() * (history.size() < style->completeListMaxSize ? static_cast<int>(history.size()) : style->completeListMaxSize));
	else
		newSize.y = static_cast<float>(style->characterSet->GetLineHeight() * (suggestions.size() < style->completeListMaxSize ? static_cast<int>(suggestions.size()) : style->completeListMaxSize));

	newSize.x = 256.0f; //TODO: Auto generate this

	completeList.SetSize(newSize);
}

void Console::SwitchCompleteListMode(COMPLETE_LIST_MODE mode)
{
	if(completeListMode != mode)
	{
		completeList.ClearElements();
		completeListMode = mode;

		historyIndex = -1;
		suggestionIndex = -1;

		if(mode == COMPLETE_LIST_MODE::HISTORY)
			MoveHistoryButtonsToCompleteList();
	}
}

void Console::TrimSuggestions(const std::string& text)
{
	std::vector<const DictionaryEntry*> newSuggestions;

	for(const DictionaryEntry* entry : suggestions)
	{
		if(entry->Matches(text))
			newSuggestions.push_back(entry);
	}

	suggestions = std::move(newSuggestions);

	suggestionButtons.erase(suggestionButtons.begin() + suggestions.size(), suggestionButtons.end());

	for(int i = 0, end = static_cast<int>(suggestions.size()); i < end; i++)
		static_cast<DGUI::Button*>(suggestionButtons[i].get())->SetText(suggestions[i]->GetName());
}

void Console::GenerateSuggestions(const std::string& text)
{
	//Need to clear here since some pointers may be removed
	completeList.ClearElements();

	suggestions = commandDictionary.Match(text);

	if(suggestions.size() > 0)
	{
		if(suggestions.size() >= suggestionButtons.size())
		{
			for(auto i = suggestionButtons.size(), end = suggestions.size(); i < end; i++)
			{
				std::unique_ptr<DGUI::Button> button = std::unique_ptr<DGUI::Button>(new DGUI::Button);

				button->Init(DLib::Rect(glm::vec2(), glm::vec2(style->completeListBackground->GetWorkArea().GetWidth(), this->style->characterSet->GetLineHeight()))
							 , style->completeListButtonStyle
							 , std::shared_ptr<DGUI::GUIBackground>(style->completeListButtonBackground->Clone())
							 , style->completeListButtonBackgroundStyle
							 , nullptr
							 , "");

				suggestionButtons.push_back(std::move(button));
			}

			for(int i = 0, end = static_cast<int>(suggestions.size()); i < end; i++)
				static_cast<DGUI::Button*>(suggestionButtons[i].get())->SetText(suggestions[i]->GetName());
		}
		else
			TrimSuggestions(text);
	}
	else
		suggestionButtons.clear();
}

void Console::HighlightCompleteListIndex(int index)
{
	if(completeListMode == COMPLETE_LIST_MODE::HISTORY)
		AcceptText(history[index]);
	else
		AcceptText(suggestions[index]->GetName());

	completeList.HighlightElement(index);
	completeList.SetIgnoreMouse(true);
}

void Console::AcceptText(const std::string& text)
{
	if(completeListMode == COMPLETE_LIST_MODE::HISTORY)
	{
		input.SetText(text);
		input.SetCursorIndex(static_cast<unsigned int>(text.size()));
	}
	else
	{
		//Local cursor index represents the cursor index of the current active character block
		unsigned int globalCursorIndex = input.GetCursorIndex();
		unsigned int characterBlockCursorIndex = 0;
		std::string currentText = input.GetActiveCharacterBlockText(characterBlockCursorIndex);
		unsigned int localCursorIndex = globalCursorIndex - characterBlockCursorIndex;

		//Construct a string with separators to turn
		//Foo(bar0,bar1) into
		//Foo
		//(
		//bar0
		//,
		//bar1
		//)
		//It is within a character block so there will be no spaces
		DLib::ConstructedString constructedString = style->characterSet->ConstructString(currentText, "(),", true);

		int currentIndex = 0;
		for(const DLib::CharacterBlock& characterBlock : constructedString.characterBlocks)
		{
			if(currentIndex + characterBlock.length >= localCursorIndex)
			{
				localCursorIndex = currentIndex;
				currentText.replace(currentIndex, characterBlock.length, text);
				break;
			}

			currentIndex += characterBlock.length;
		}

		input.ReplaceActiveCharacterBlockText(currentText);
		input.SetCursorIndex(characterBlockCursorIndex + localCursorIndex + static_cast<unsigned int>(text.size()));
	}
}

DGUI::GUIStyle* Console::GenerateDoomStyle(DLib::ContentManager* contentManager)
{
	ConsoleStyle* style = new ConsoleStyle();

	style->characterSet = contentManager->Load<DLib::CharacterSet>("Calibri16");
	style->padding = glm::vec2(0.0f, 0.0f);
	style->inputOutputPadding = 2.0f;
	style->allowResize = false;
	style->allowMove = false;
	style->labelText = ">";

	std::shared_ptr<DGUI::ScrollbarStyle> scrollbarStyle = std::make_shared<DGUI::ScrollbarStyle>();

	//////////////////////////////////////////////////
	//LABEL
	//////////////////////////////////////////////////
	std::shared_ptr<DGUI::LabelStyle> labelStyle = std::make_shared<DGUI::LabelStyle>();

	labelStyle->characterSet = style->characterSet;
	labelStyle->textColor = DGUI::COLORS::black;

	style->labelStyle = labelStyle;

	//////////////////////////////////////////////////
	//OUTPUT
	//////////////////////////////////////////////////
	std::shared_ptr<DGUI::TextFieldStyle> outputStyle = std::make_shared<DGUI::TextFieldStyle>();
	outputStyle->textColorNormal = DGUI::COLORS::black;
	outputStyle->characterSet = style->characterSet;
	outputStyle->cursorSize = glm::vec2(2.0f, 16.0f);

	//Scrollbar
	scrollbarStyle->thumbColor = DGUI::COLORS::gray66;
	scrollbarStyle->thumbWidth = 8;
	scrollbarStyle->thumbMinSize = 16;

	outputStyle->scrollbarBackground = std::make_shared<DGUI::EmptyBackground>();
	outputStyle->scrollbarBackgroundStyle = nullptr;
	outputStyle->scrollBarStyle = scrollbarStyle;

	style->outputStyle = outputStyle;
	style->outputBackground = std::make_shared<DGUI::EmptyBackground>();
	style->outputBackgroundStyle = nullptr;

	//////////////////////////////////////////////////
	//INPUT
	//////////////////////////////////////////////////
	auto inputStyle = std::make_shared<DGUI::TextBoxStyle>();
	inputStyle->textColorNormal = DGUI::COLORS::white;
	inputStyle->characterSet = style->characterSet;
	inputStyle->cursorSize = glm::vec2(2.0f, 16.0f);
	inputStyle->cursorColorNormal = DGUI::COLORS::white;

	style->inputStyle = inputStyle;
	style->inputBackground = std::make_shared<DGUI::EmptyBackground>();
	style->inputBackgroundStyle = nullptr;

	//////////////////////////////////////////////////
	//COMPLETELIST
	//////////////////////////////////////////////////
	auto completeListBackgroundStyle = std::make_shared<DGUI::OutlineBackgroundStyle>();
	completeListBackgroundStyle->backgroundColors.emplace_back(DGUI::COLORS::gray50);
	completeListBackgroundStyle->outlineColors.emplace_back(DGUI::COLORS::gray66);
	completeListBackgroundStyle->outlineSides = DGUI::DIRECTIONS::BOTTOM;// | DGUI::DIRECTIONS::LEFT | DGUI::DIRECTIONS::RIGHT;
	completeListBackgroundStyle->inclusiveBorder = false;
	completeListBackgroundStyle->outlineThickness = 2.0f;

	auto completeListStyle = std::make_shared<DGUI::ListStyle>();
	completeListStyle->scrollbarBackground = std::make_shared<DGUI::EmptyBackground>();
	completeListStyle->scrollbarBackgroundStyle = nullptr;
	completeListStyle->scrollbarStyle = scrollbarStyle;

	style->completeListStyle = completeListStyle;
	style->completeListBackground = std::make_shared<DGUI::OutlineBackground>();
	style->completeListBackgroundStyle = completeListBackgroundStyle;

	auto buttonStyle = std::make_shared<DGUI::ButtonStyle>();

	buttonStyle->textColors[static_cast<int>(DGUI::ButtonStyle::BUTTON_STATES::NORMAL)] = DGUI::COLORS::black;
	buttonStyle->textColors[static_cast<int>(DGUI::ButtonStyle::BUTTON_STATES::CLICK)] = DGUI::COLORS::black;
	buttonStyle->textColors[static_cast<int>(DGUI::ButtonStyle::BUTTON_STATES::HOVER)] = DGUI::COLORS::black;

	buttonStyle->characterSet = style->characterSet;

	style->completeListButtonStyle = buttonStyle;

	auto buttonBackgroundStyle = std::make_shared<DGUI::OutlineBackgroundStyle>();

	//Normal, Click, Hover
	buttonBackgroundStyle->backgroundColors.resize(static_cast<int>(DGUI::ButtonStyle::BUTTON_STATES::SIZE));
	buttonBackgroundStyle->backgroundColors[static_cast<int>(DGUI::ButtonStyle::BUTTON_STATES::NORMAL)] = DGUI::COLORS::transparent;
	buttonBackgroundStyle->backgroundColors[static_cast<int>(DGUI::ButtonStyle::BUTTON_STATES::CLICK)] = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
	buttonBackgroundStyle->backgroundColors[static_cast<int>(DGUI::ButtonStyle::BUTTON_STATES::HOVER)] = DGUI::COLORS::darkgray;

	style->completeListButtonBackgroundStyle = buttonBackgroundStyle;
	style->completeListButtonBackground = std::make_shared<DGUI::OutlineBackground>();

	return style;
}

DGUI::GUIBackground* Console::GenerateDoomStyleBackground(DLib::ContentManager* contentManager)
{
	return new DGUI::OutlineBackground();
}

DGUI::GUIStyle* Console::GenerateDoomStyleBackgroundStyle(DLib::ContentManager* contentManager)
{
	DGUI::OutlineBackgroundStyle* style = new DGUI::OutlineBackgroundStyle();

	style->backgroundColors.emplace_back(DGUI::COLORS::gray50);
	style->outlineColors.emplace_back(DGUI::COLORS::gray66);
	style->outlineSides = DGUI::DIRECTIONS::BOTTOM;
	style->inclusiveBorder = false;
	style->outlineThickness = 2.0f;

	return style;
}

std::string Console::GenerateSuggestionText()
{
	unsigned int localCursorIndex = 0;
	std::string text = input.GetActiveCharacterBlockText(localCursorIndex);
	localCursorIndex = input.GetCursorIndex() - localCursorIndex;

	DLib::ConstructedString constructedString = style->characterSet->ConstructString(text, "(),", true);

	int currentIndex = 0;
	for(const DLib::CharacterBlock& characterBlock : constructedString.characterBlocks)
	{
		if(currentIndex + characterBlock.length >= localCursorIndex)
			return text.substr(currentIndex, characterBlock.length);

		currentIndex += characterBlock.length;
	}

	//This shouldn't be reached
	return "";
}

bool Console::VerifyCommandName(const string& name)
{
	if(!isalpha(name[0]))
		return false;

	for(auto character : name)
	{
		if(!isalpha(character)
				&& !isdigit(character)
				&& character != '_')
		{
			return false;
		}
	}

	return true;
}